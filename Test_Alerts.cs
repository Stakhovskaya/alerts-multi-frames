using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;
using System.Threading;


namespace Tests
{

    public class Tests
    {
        
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            IWebDriver driver;
            driver = new ChromeDriver();
            driver.Url = "https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/";

            //------Simple alert button------
            IWebElement element11 = driver.FindElement(By.XPath("//descendant::a[contains(@id, 'cookie')]"));
            element11.Click();
            IWebElement element1 = driver.FindElement(By.XPath("//button [text() = 'Simple Alert']"));
            element1.Click();
            Console.WriteLine(@"Кнопка 'Simple Alert' была успешно нажата");
            IAlert simpleAlert = driver.SwitchTo().Alert();
            simpleAlert.Accept();
            
            //------Confirm pop up button----
            IWebElement element2 = driver.FindElement(By.XPath("//button [text() = 'Confirm Pop up']"));

            element2.Click();
            Console.WriteLine(@"Кнопка 'Confirm pop up' была успешно нажата");
            IAlert confirmationAlert = driver.SwitchTo().Alert();
            String AlertText = confirmationAlert.Text;
            confirmationAlert.Accept();
            Console.WriteLine(@"В 'Confirm pop up' было нажато 'ОК'");
            element2.Click();
            driver.SwitchTo().Alert();
            confirmationAlert.Dismiss();
            Console.WriteLine(@"В 'Confirm pop up' была нажата 'отмена'");

            //------Prompt Alert Button------
            IWebElement element3 = driver.FindElement(By.XPath("//button [text() = 'Prompt Pop up']"));
            element3.Click();
            Console.WriteLine(@"Кнопка 'Prompt Alert Button' была успешно нажата");
            IAlert promptAlert = driver.SwitchTo().Alert();
            String Alerttext = promptAlert.Text;
            promptAlert.SendKeys("Great site");
            promptAlert.Accept();

            driver.Quit();

        }
    }
}