﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace Tests
{
    public class Multi_Window_Tests //: createDriver
    {
        IWebDriver driver;
        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void Test2() 
        {
            driver.Url = "https://catalog.onliner.by/tv";
            IWebElement element1 = driver.FindElement(By.XPath("//descendant::a[contains (@href, 'apple')]"));
            element1.Click();
            //switch to the 1-st window.
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            IWebElement element2 = driver.FindElement(By.XPath("//descendant::a[contains (@href, 'play.google')]"));
            element2.Click();
            driver.WindowHandles.ToList();
   
            string PageTitle = driver.FindElement(By.XPath("//span[contains(text(),'Каталог Onliner')]")).Text;
            string ExpectedTitle = "Каталог Onliner";
            Assert.AreEqual(ExpectedTitle, PageTitle);

            IWebElement wordMore = driver.FindElement(By.XPath("//a[contains(text(),'Ещё') and contains (@href, '/store/apps/col')]"));
            wordMore.Click();
            Console.WriteLine("There are 58 similar applications");
            
            driver.Navigate().GoToUrl("https://apps.apple.com/app/apple-store/id1062235600");
            IWebElement moreButton = driver.FindElement(By.XPath("//html/body/div[4]/div/main/div/div/div/div[2]/section[3]/div/div/div/button"));
            moreButton.Click();
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles.First());
            IWebElement marketing = driver.FindElement(By.XPath("//div[contains(@id,'adfox_157649')]"));
            marketing.Click();


            driver.Quit();
        }
    }
}
